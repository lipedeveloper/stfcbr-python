import json
from flask import Flask, request
import funct
import boto3
from trp import Document

app = Flask(__name__)


@app.route('/')
def home():
  return 'Started'

@app.route('/ocr', methods=['POST'])
def base64img():
    base64 = request.json.get('base64')
    funct.receberimage(base64)
    # Document
    documentName = "imagetoSave.png"

    # Amazon Textract client
    textract = boto3.client('textract')

    # Call Amazon Textract
    with open(documentName, "rb") as document:
        response = textract.analyze_document(
            Document={
                'Bytes': document.read(),
            },
            FeatureTypes=["TABLES"])

    # print(response)

    doc = Document(response)
    data = []
    for page in doc.pages:
        # Print tables
        for table in page.tables:
            for r, row in enumerate(table.rows):
                myDictObj = {"name": row.cells[2].text.strip(), "power": row.cells[3].text.strip()}
                data.append(myDictObj)

    return json.dumps(data, sort_keys=True, indent=3)

